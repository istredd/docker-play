FROM nginx:mainline-alpine
RUN rm /etc/nginx/conf.d/*
ADD nginx/hello.conf /etc/nginx/conf.d/
ADD nginx/index.html /usr/share/nginx/html/
